from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os #provides ways to access the Operating System and allows us to read the environment variables

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=("neo4j", "test1234"),database="neo4j")

def get_all_employees(tx, sort_by=None, filter_by=None):
    query = "MATCH (n:Employee)"

    if filter_by:
        query += f" WHERE {filter_by}"

    query += " RETURN n"

    if sort_by:
        query += f" ORDER BY n.{sort_by}"

    results = tx.run(query).data()

    employyes = [{'name': result['n']['name'] for result in results}]

    return employyes

@app.route('/employees', methods=['GET'])
def get_employees():
    sort_by = request.args.get('sort_by')
    filter_by = request.args.get('filter_by')

    with driver.session() as session:
        result = session.read_transaction(get_all_employees, sort_by, filter_by)
        return jsonify({'employees': result}), 200
    

def create_new_employee(tx, name, department, stanowisko):
    result = tx.run("MATCH (e:Employee {name: $name}) RETURN e", name=name)
    if result.single():
        return "Employee with this name already exists"

    query = f"""
    MATCH (d:Department {{name: $department}})
    CREATE (e:Employee {{name: $name}})-[:{stanowisko}]->(d);
    """
    tx.run(query)

@app.route('/employees', methods=['POST'])
def create_employee():
    name = request.json.get('name')
    department = request.json.get('department')
    stanowisko = request.json.get('stanowisko')

    if not all([name, department, stanowisko]):
        return jsonify({"message": "Missing arguments"}), 400

    with driver.session() as session:
        message = session.write_transaction(create_new_employee, name, department, stanowisko)
        if message:
            return jsonify({"message": message}), 400
        else:
            return jsonify({"message": "Employee created"}), 201
    
def edit_employee_helper(tx, new_name, new_department, new_stanowisko, id):
    query = f"""
    MATCH (e:Employee)-[r]->(d:Department)
    WHERE id(e) = {id}
    DELETE r
    WITH e
    MATCH (d2:Department {{name: '{new_department}'}})
    CREATE (e)-[:{new_stanowisko}]->(d2)
    SET e.name = '{new_name}'
    """
    tx.run(query)

@app.route('/employees/<int:id>', methods=['PUT'])
def edit_employee(id):
    new_name = request.json['name']
    new_department = request.json['department']
    new_stanowisko = request.json['stanowisko']

    with driver.session() as session:
        session.write_transaction(edit_employee_helper, new_name, new_department, new_stanowisko, id)
        return jsonify({"message": "Employee edited"}), 201
    
def delete_employee_helper(tx, id):
    result = tx.run(f"MATCH (e:Employee)-[r:MANAGES]->() WHERE id(e) = {id} RETURN r")
    if result.single():
        return "Cannot delete employee because they have a MANAGES position"

    tx.run(f"MATCH (e:Employee) WHERE id(e) = {id} DETACH DELETE e")

@app.route('/employees/<int:id>', methods=['DELETE'])
def delete_employee(id):
    with driver.session() as session:
        message = session.write_transaction(delete_employee_helper, id)
        if message:
            return jsonify({"message": message}), 400
        else:
            return jsonify({"message": "Employee deleted"}), 200

def get_subordinates_helper(tx, id):
    query = f"""
    MATCH (e:Employee)-[:MANAGES*]->(d:Department)<-[:WORKS_IN]-(sub:Employee)
    WHERE id(e) = {id}
    RETURN sub
    """
    results = tx.run(query).data()
    subordinates = [{'name': result['sub']['name']} for result in results]
    return subordinates

@app.route('/employees/<int:id>/subordinates', methods=['GET'])
def get_subordinates(id):
    with driver.session() as session:
        result = session.read_transaction(get_subordinates_helper, id)
        return jsonify({'subordinates': result}), 200


def get_department_helper(tx, id):
    query = f"""
    MATCH (d:Department)<-[:WORKS_IN]-(o:Employee)
    WHERE id(d) = {id}
    WITH d, count(o) AS employee_count
    MATCH (d)<-[:MANAGES]-(manager:Employee)
    RETURN d.name AS department, employee_count, collect(manager.name) AS managers
    """
    result = tx.run(query).single()
    return {"department": result["department"], "employee_count": result["employee_count"], "managers": result["managers"]}

@app.route('/department/<int:id>/', methods=['GET'])
def get_department(id):
    with driver.session() as session:
        result = session.read_transaction(get_department_helper, id)
        return jsonify(result), 200


def get_departments_helper(tx, sort_by=None, filter_by=None):
    query = "MATCH (d:Department)<-[:WORKS_IN]-(o:Employee) "
    if filter_by:
        query += f"WHERE d.name CONTAINS '{filter_by}' "
    query += "RETURN d.name AS department, count(o) AS employee_count "
    if sort_by:
        query += f"ORDER BY {sort_by}"
    results = tx.run(query)
    return [{"department": record["department"], "employee_count": record["employee_count"]} for record in results]

@app.route('/departments', methods=['GET'])
def get_departments():
    sort_by = request.args.get('sort_by')
    filter_by = request.args.get('filter_by')
    with driver.session() as session:
        results = session.read_transaction(get_departments_helper, sort_by, filter_by)
        return jsonify(results), 200
    


def get_department_employees_helper(tx, id):
    query = f"""
    MATCH (d:Department)<-[:WORKS_IN]-(e:Employee)
    WHERE id(d) = {id}
    RETURN d.name as department, collect(e.name) AS employees
    """
    result = tx.run(query).single()
    return {"department": result["department"], "employees": result["employees"]}

@app.route('/departments/<int:id>/employees', methods=['GET'])
def get_department_employees(id):
    with driver.session() as session:
        employees = session.read_transaction(get_department_employees_helper, id)
        return jsonify(employees), 200
    

if __name__ == '__main__':
    app.run()



